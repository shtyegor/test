export default {
  required: value => {
    return !value ? 'Please fill out this field' : ''
  },
  email: email => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return !re.test(String(email).toLowerCase()) ? 'Enter a valid email' : ''
  },
  minLen: (value, min) => {
    return String(value).length < min ? `Minimum length of field ${min}` : ''
  },
  maxLen: (value, max) => {
    return String(value).length > max ? `Maximum length of field ${max}` : ''
  },
  pass: pass => {
    const re = /^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]))/
    return !re.test(String(pass)) ? 'Password must contain at least one uppercase, one lowercase and a number' : ''
  },
  samePass: (pass1, pass2) => {
    return pass1 != pass2 ? 'Passwords do not match' : ''
  },
  dateComparison: (start, end) => {
    return new Date(start) > new Date(end) ? 'End date cannot be before start date': ''
  },
  agree: bool => {
    return !bool ? 'Please agree': ''
  }
}
