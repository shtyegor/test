export const state = () => ({
  token: null
})

export const mutations = {
  set(state, {name, value}) {
    state[name] = value
  }
}

export const getters = {
  get: state => name => state[name]
}
