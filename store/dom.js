export const state = () => ({
  pageHidden: false
})

export const mutations = {
  setPageHidden(state, bool) {
    state.pageHidden = bool
  }
}

export const actions = {

}

export const getters = {
  isPageHidden: state => state.pageHidden
}
