export const state = () => ({
  products: [],
  loading: false,
  popupProductId: null
})

export const mutations = {
  addProducts(state, products) {
    state.products = state.products.concat(products)
  },
  deleteProducts(state) {
    state.products = []
  },
  setLoading(state, bool) {
    state.loading = bool
  },
  setPopupProductId(state, id) {
    state.popupProductId = id
  }
}

export const actions = {
  async fetch({rootGetters, commit}, page) {
    commit('setLoading', true)

    const token = rootGetters['auth/token']

    await this.$axios
      .get(`${process.env.apiUrl}/products`, {
        headers: {
          'Accept': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      .then(({data}) => {
        commit('addProducts', data)
      })
      .catch(error => {
        console.log(error)
      })

    commit('setLoading', false)
  }
}

export const getters = {
  items: state => state.products,
  isLoading: state => state.loading,
  popupProductId: state => state.popupProductId,
  getProductById: state => id => {
    let product = null
    state.products.forEach(p => {
      if(p.id == id) {
        product = p
        return
      }
    })
    return product
  }
}
