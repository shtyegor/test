export const state = () => ({
  login: false,
  register: false,
  product: false
})

export const mutations = {
  openPopup(state, name) {
    state[name] = true
  },
  closePopup(state, name) {
    state[name] = false
  }
}

export const actions = {

}

export const getters = {
  isPopup: state => name => state[name]
}
