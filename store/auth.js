export const state = () => ({
  token: null
})

export const mutations = {
  setToken(state, token) {
    state.token = token
  }
}

export const actions = {
  async login({commit, dispatch}, {email, password}) {
    return await this.$axios
      .post(`${process.env.apiUrl}/login`, {
        email,
        password
      })
      .then(({data}) => {
        commit('setToken', data.accessToken)
        commit('sessionStorage/set', { name: 'user', value: data.accessToken }, {root: true})
        dispatch('products/fetch', null, {root: true})
        return {
          success: true
        }
      })
      .catch(error => {
        const res = JSON.parse(error.response.request.response)
        const message = res && res.message ? res.message : error.message
        const errors = res && res.errors ? res.errors : {}

        return  {
          success: false,
          message: message,
          errors: errors
        }
      })
  },

  async register({commit, dispatch}, {name, email, education_start_date, education_end_date, password, password_confirmation, terms}) {
    return await this.$axios
      .post(`${process.env.apiUrl}/register`, {
        name,
        email,
        education_start_date,
        education_end_date,
        password,
        password_confirmation,
        terms
      })
      .then(({data}) => {
        commit('setToken', data.accessToken)
        commit('sessionStorage/set', { name: 'user', value: data.accessToken }, {root: true})
        dispatch('products/fetch', null, {root: true})
        return {
          success: true
        }
      })
      .catch(error => {
        const res = JSON.parse(error.response.request.response)
        const message = res && res.message ? res.message : error.message
        const errors = res && res.errors ? res.errors : {}

        return  {
          success: false,
          message: message,
          errors: errors
        }
      })
  },

  logout({commit, getters}) {
    const token = getters.token
    this.$axios
      .post(
        `${process.env.apiUrl}/logout`,
        {},
        {
          headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`
          }
        }
      )
      .then(() => {
        commit('setToken', null)
        commit('products/deleteProducts', null, {root: true})
        commit('sessionStorage/set', { name: 'user', value: null }, {root: true})
      })
  }
}

export const getters = {
  token: state => state.token,
  authenticated: state => !!state.token
}
