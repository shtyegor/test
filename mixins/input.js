export default {
  props: {
    name: String,
    placeholder: String,
    error: String
  },
  data: () => {
    return {
      value: '',
      isFocus: false,
      randomNum: Math.floor(Math.random() * Math.floor(10000000))
    }
  },
  watch: {
    value(val) {
      this.$emit('changeValue', val)
    }
  },
  computed: {
    id() {
      return `input-${this.randomNum}`
    },
    isActive() {
      return this.isFocus || !!this.value
    },
    isError() {
      return !!this.error
    }
  }
}
